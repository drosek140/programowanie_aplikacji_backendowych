package com.example.demo;

public class UserEntity {

    private Integer wiek;
    private String imie;
    private String nazwisko;

    public UserEntity(String imie, String nazwisko,Integer wiek){
        this.wiek=wiek;
        this.imie=imie;
        this.nazwisko=nazwisko;
    }
    public int getWiek(){return wiek;}
    public void setWiek(int wiek){this.wiek=wiek;}
    public String getImie(){return imie;}
    public void setImie(String imie){this.imie=imie;}
    public String getLastName(){return nazwisko;}
    public void setLastName(String nazwisko){this.nazwisko=nazwisko;}




}
