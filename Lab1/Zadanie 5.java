package com.company;

import java.time.LocalTime;
import java.util.Date;

public class Main {

    public static void main(String[] args) {

        LocalTime localTime = LocalTime.now();
        System.out.println("data lokalna " + localTime);

        Date globalTime = new Date();
        System.out.println("data globalna " + globalTime);
    }
}
