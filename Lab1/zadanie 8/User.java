package com.company;

public  class User {

    private String name;
    private int age;

    // required no-arguments constructor
    public User() {
        // nothing here ...
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }
}